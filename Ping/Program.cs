﻿using System;
using RabbitMQ.Wrapper;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new RabbitClient("localhost", false);
            client.SendMessageToQueue();
            client.ListenQueue();

            Console.ReadKey();
        }
    }
}
