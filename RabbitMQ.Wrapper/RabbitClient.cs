﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Threading;

namespace RabbitMQ.Wrapper
{
    public class RabbitClient : IDisposable
    {
        private ConnectionFactory _factory;
        private IConnection _connection;
        private IModel _channelSend;
        private IModel _channelRecive;
        private string _sendTo;
        private string _reciveFrom;
        private string _message;
        private double _timer;

        // type true - Ponger, false - Pinger
        public RabbitClient(string host, bool type, double timer = 2.5)
        {
            _factory = new ConnectionFactory() { HostName = host };
            _connection = _factory.CreateConnection();
            _channelSend = _connection.CreateModel();
            _channelRecive = _connection.CreateModel();
            _timer = timer;


            if (type)
            {
                _sendTo = "ping_queue";
                _reciveFrom = "pong_queue";
                _message = "pong";
            }
            else
            {
                _sendTo = "pong_queue";
                _reciveFrom = "ping_queue";
                _message = "ping";
            }

            _channelSend.QueueDeclare(
                    queue: _sendTo,
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);
            _channelRecive.QueueDeclare(
                    queue: _reciveFrom,
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

            _channelSend.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
            _channelRecive.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
        }
        public void SendMessageToQueue()
        {
            var body = Encoding.UTF8.GetBytes(_message);

            _channelSend.BasicPublish(exchange: "",
                                 routingKey: _sendTo,
                                 basicProperties: null,
                                 body: body);
        }

        public void ListenQueue()
        {
            var consumer = new EventingBasicConsumer(_channelRecive);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                Console.WriteLine("{0} | {1}", DateTime.Now, message);
                Thread.Sleep(Convert.ToInt32(_timer * 1000));

                _channelRecive.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);

                SendMessageToQueue();

            };

            _channelRecive.BasicConsume(queue: _reciveFrom,
                                 autoAck: false,
                                 consumer: consumer);
        }

        public void Dispose()
        {
            _channelRecive.Close();
            _channelSend.Close();
            _channelRecive.Dispose();
            _channelSend.Dispose();
            _connection.Close();
            _connection.Dispose();
        }
    }
}
