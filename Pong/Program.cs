﻿using System;
using RabbitMQ.Wrapper;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new RabbitClient("localhost", true);
            client.ListenQueue();

            Console.ReadKey();
        }
    }
}
